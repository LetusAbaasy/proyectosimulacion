$(document).ready(function(){
    var $aLlegada = 2;
    var $bLlegada = 8;
    var $aAtencion = 3;
    var $bAtencion = 6;
    var $cAtencion = 10;
    var $xAtencion = 0;
    var $nRan1 = 0;
    var $nRan2 = 0;
    var $cajeros = [];
    var $atenciones = [];
    var $clientes = [];
    var $clientesAtendidos = [];
    var $cliente = 0;
    var $totalAtencion = 0;
    var $totalizarLlegada = 0;
    var $minuto = 0;

    $("#simular").click(function(){
        //______ Slides
        $("#problema").slideUp();
        $(".datos").slideDown();
        // ____ Slides
        //___ Variables valor
        $numeroCajeros = $("#numeroCajeros").val();
        $minutos = $("#minutos").val();
        // ____Añador variables
        $("#datosNumeroCajeros").append($numeroCajeros);
        $("#datosMinutos").append($minutos);

        // Crear cajeros
        for($c = 0; $c < $numeroCajeros; $c++){
            $cajeros.push("Disponible");
        }

        do{
            $nRanAtencion = Math.random().toFixed(2); //Math.floor((Math.random() * 8) + 3);
            $nRanLlegada = Math.random().toFixed(2); //Math.floor((Math.random() * 2) + 8);
            $nRan1 = Math.random().toFixed(2); // Numero aleatorio numero 1
            $nRan2 = Math.random().toFixed(2); // Numero aleatorio numero 2
            $randomLlegada = $aLlegada + ($bLlegada - $aLlegada) * $nRanLlegada; // x = a + (b - a) * R -> Distribucion Uniforme.
            $randomLlegada = $randomLlegada.toFixed(0);
            $totalizarLlegada += parseInt($randomLlegada);

            if ($("#uso1").is(':checked')) {
                console.log("Usando Integral Inversa");
                // Integral Inversa
                if($nRan1 < (3/7)){
                    $xAtencion = ((6+(Math.sqrt(36-(84*$nRan1))))/2);
                }else if($nRan1 > (3/7)){
                    $xAtencion = (10-(Math.sqrt($nRan1+28)));
                }
                $("#method").html("Usando Integral Inversa");
            }else if($("#uso2").is(':checked')){
                console.log("Usando Distribucion Triangular");
                // Distribucion Triangular
                if($nRan1 < (($bAtencion - $aAtencion) / ($cAtencion - $aAtencion))){
                    $xAtencion = $aAtencion + ($bAtencion - $aAtencion) * Math.sqrt($nRan2); // Valida si se usa F1(x)
                }else{
                    $xAtencion = $cAtencion - ($cAtencion - $bAtencion) * Math.sqrt($nRan2); // Valida si no se cumple para F1(x)
                }
                $("#method").html("Usando Distribucion Triangular");
            }

            $xAtencion = parseInt($xAtencion.toFixed(0));
            $atenciones.push($randomLlegada);
            // ________________________
            // ________________________

            $cliente = $cliente+1;
            $clientes.push({
                "Cliente":$cliente,
                "Llegada":$totalizarLlegada,
                "Llego":$totalizarLlegada,
                "At":$xAtencion,
                "nRanLlegada": $nRanLlegada,
                "nRan1": $nRan1,
                "randomLlegada": $randomLlegada,
                "estado": "No Atendido"
            });

            for($j = 0; $j < $clientes.length; $j++){
                if($clientes[$j].estado == "No Atendido"){
                    for($k = 0; $k < $cajeros.length; $k++){
                        if($cajeros[$k] == "Disponible"){
                            $cajeros[$k] = {
                                "Cliente":$clientes[$j].Cliente,
                                "Atencion":parseInt($clientes[$j].At),
                                "Demora":parseInt($clientes[$j].At),
                                "Llego":parseInt($clientes[$j].Llego)
                            };

                            $clientes[$j].estado = "Atendido por cajero #"+($k + 1);
                            $clientesAtendidos.push({"Cliente Atendido":$clientes[$j].Cliente, "CajeroAtendio": ($k + 1)});
                            if(($cajeros[$k].Atencion + $cajeros[$k].Llego) == ($clientes[$j].Llegada + $clientes[$j].At)){
                                $cajeros[$k] = "Disponible";
                            }
                        }
                    }
                }else{
                    //$j++;
                }
            }
        }while($totalizarLlegada <= $minutos);

        for($i = 0; $i < $minutos; $i++){
            $minuto = $minuto+1;
            $(".tres").append("<tr style='background-color:#DC2B30;' id=tr"+$minuto+$c+$i+">");
            $(".tres>#tr"+$minuto+$c+$i).append("<td id='min"+$minuto+$c+"'>"+$minuto+"</td>");
           /* $(".tres>#tr"+$minuto+$c+$i).append("<td id='clie"+$minuto+$c+"'>-</td>");
            $(".tres>#tr"+$minuto+$c+$i).append("<td id='nran"+$minuto+$c+"'>-</td>");
            $(".tres>#tr"+$minuto+$c+$i).append("<td id='ranll"+$minuto+$c+"'>-</td>");
            $(".tres>#tr"+$minuto+$c+$i).append("<td id='tll"+$minuto+$c+"'>-</td>");
            $(".tres>#tr"+$minuto+$c+$i).append("<td id='ta"+$minuto+$c+"'>-</td>");
            $(".tres>#tr"+$minuto+$c+$i).append("<td id='ts"+$minuto+$c+"'>-</td>");
            $(".tres>#tr"+$minuto+$c+$i).append("<td id='ta"+$minuto+$c+"'>-</td>");
            $(".tres>#tr"+$minuto+$c+$i).append("<td id='est"+$minuto+$c+"'>-</td>");*/
            $(".tres").append("</tr>");
            for($c = 0; $c < $clientes.length; $c++){
                if($minuto == $clientes[$c].Llego){
                    $(".tres").append("<tr style='background-color:#8A9FC6;' id=tr"+$minuto+$c+$i+">");
                    $(".tres>#tr"+$minuto+$c+$i).append("<td style='background-color:#4BD230;' id='min"+$minuto+$c+"'>"+$minuto+"</td>");
                    $(".tres>#tr"+$minuto+$c+$i).append("<td style='background-color:#4BD230;' id='clie"+$minuto+$c+"'>"+$clientes[$c].Cliente+"</td>");
                    $(".tres>#tr"+$minuto+$c+$i).append("<td style='background-color:#00A7CB;' id='nran"+$minuto+$c+"'>"+$clientes[$c].nRanLlegada+"</td>");
                    $(".tres>#tr"+$minuto+$c+$i).append("<td style='background-color:#00A7CB;'id='ranll"+$minuto+$c+"'>"+$clientes[$c].randomLlegada+"</td>");
                    $(".tres>#tr"+$minuto+$c+$i).append("<td style='background-color:#00A7CB;'id='tll"+$minuto+$c+"'>"+$clientes[$c].Llegada+"</td>");
                    $(".tres>#tr"+$minuto+$c+$i).append("<td id='te"+$minuto+$c+"'>"+($clientes[$c].Llegada - $clientes[$c].At)+"</td>");
                    $(".tres>#tr"+$minuto+$c+$i).append("<td style='background-color:#FFA100;' id='ranat"+$minuto+$c+"'>"+$clientes[$c].nRan1+"</td>");
                    $(".tres>#tr"+$minuto+$c+$i).append("<td style='background-color:#FFA100;' id='ta"+$minuto+$c+"'>"+$clientes[$c].At+"</td>");
                    $(".tres>#tr"+$minuto+$c+$i).append("<td id='ts"+$minuto+$c+"'>"+($clientes[$c].Llegada+$clientes[$c].At)+"</td>");
                    $(".tres>#tr"+$minuto+$c+$i).append("<td style='background-color:#FFA100;' id='est"+$minuto+$c+"'>Atendio el Cajero: #"+$clientesAtendidos[$c].CajeroAtendio+"</td>");
                    $(".tres").append("</tr>");
                }
            }
        }
        console.log("Cajeros:");
        console.log($cajeros);
        console.log("Clientes:");
        console.log($clientes);
        console.log("Clientes Atendidos:");
        console.log($clientesAtendidos);

        imprimirDatos("Numero de Clientes: "+($clientes.length - 1)+".");
        imprimirDatos("Numero de Clientes Atendidos: "+(($clientesAtendidos.length - 1) / $cajeros.length).toFixed(0)+".");
        imprimirDatos("Tiempo Espera Promedio: "+($totalizarLlegada / $clientesAtendidos.length)+".");
        imprimirDatos("Tiempo de Llegada Promedio: "+($totalizarLlegada / $clientes.length)+".");
    });

    function imprimirDatos($data){
        $(".resultados").append($data+"<br/>");
    }
});